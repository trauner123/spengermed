﻿using System;

namespace Spengermed
{
    class Program
    {
        static void Main(string[] args)
        {
            LeseEingabe();
        }

        static void LeseEingabe()
        {
            int alter;
            int groesse;
            int druck;
            try
            {
                Console.Write("Eingabe Alter zwischen 17 und 24 ein: ");
                alter = Convert.ToInt32(Console.ReadLine());
                Console.Write("Eingabe Körpergröße in cm: ");
                groesse = Convert.ToInt32(Console.ReadLine());
                Console.Write("Eingabe Geschlecht m/w: ");
                char geschlecht = Convert.ToChar(Console.ReadLine());
                Console.Write("Händedruck in kg: ");
                druck = Convert.ToInt32(Console.ReadLine());
                
                if (alter >= 17 && alter <= 19)
                {
                    if (groesse >= 150 && groesse <= 154)
                    {
                        if (druck <= 27.8)
                        {
                            Console.Write("Risikowert beträgt: 21.6, ein Gesundheitscheck wäre nicht schlecht");
                            
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                            
                        }
                    }
                    else if (groesse >= 155 && groesse <= 159)
                    {
                        if (druck <= 29.2)
                        {
                            Console.Write("Risikowert beträgt: 22.9, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                    else if (groesse >= 160 && groesse <= 164)
                    {
                        if (druck <= 30.2)
                        {
                            Console.Write("Risikowert beträgt: 24.0, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                    else if (groesse >= 165 && groesse <= 169)
                    {
                        if (druck <= 31.2)
                        {
                            Console.Write("Risikowert beträgt: 25.0, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                    else if (groesse >= 170 && groesse <= 174)
                    {
                        if (druck <= 32.2)
                        {
                            Console.Write("Risikowert beträgt: 26.0, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                    else if (groesse >= 175 && groesse <= 179)
                    {
                        if (druck <= 33.0)
                        {
                            Console.Write("Risikowert beträgt: 26.7, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                    else if (groesse >= 180 && groesse <= 184)
                    {
                        if (druck <= 33.8)
                        {
                            Console.Write("Risikowert beträgt: 27.6, ein Gesundheitscheck wäre nicht schlecht");
                        }
                        else
                        {
                            Console.Write("Sterberisiko ist gering");
                        }
                    }
                }
                else if (alter >= 20 && alter <= 24)
                    {
                    if (groesse <= 150 && groesse >= 154)
                        {
                        if (druck <= 29.1)
                            {
                            Console.Write("Risikowert beträgt: 23.7, ein Gesundheitscheck wäre nicht schlecht");
                            }
                        else
                            {
                            Console.Write("Sterberisiko ist gering");
                            }
                        }
                    }
                else if (groesse >= 155 && groesse <= 159)
                {
                    if (druck <= 30.2)
                    {
                        Console.Write("Risikowert beträgt: 24.8, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }
                else if (groesse >= 160 && groesse <= 164)
                {
                    if (druck <= 31.5)
                    {
                        Console.Write("Risikowert beträgt: 26.1, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }
                else if (groesse >= 165 && groesse <= 169)
                {
                    if (druck <= 32.5)
                    {
                        Console.Write("Risikowert beträgt: 27.1, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }
                else if (groesse >= 170 && groesse <= 174)
                {
                    if (druck <= 33.4)
                    {
                        Console.Write("Risikowert beträgt: 28.0, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }
                else if (groesse >= 175 && groesse <= 179)
                {
                    if (druck <= 34.5)
                    {
                        Console.Write("Risikowert beträgt: 28.0, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }
                else if (groesse >= 180 && groesse <= 184)
                {
                    if (druck <= 35.0)
                    {
                        Console.Write("Risikowert beträgt: 29.6, ein Gesundheitscheck wäre nicht schlecht");
                    }
                    else
                    {
                        Console.Write("Sterberisiko ist gering");
                    }
                }

                Console.ReadLine();
            }
            catch
            {
                Console.Clear();
                LeseEingabe();
            }
        }
    }
}
